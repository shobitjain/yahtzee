defmodule Yahtzee do
  def score_upper(dice) do
    %{Ones: length(Enum.filter(dice, fn e -> e == 1 end)),
    Twos: length(Enum.filter(dice, fn e -> e == 2 end)),
    Threes: length(Enum.filter(dice, fn e -> e == 3 end)),
    Fours: length(Enum.filter(dice, fn e -> e == 4 end)),
    Fives: length(Enum.filter(dice, fn e -> e == 5 end)),
    Sixes: length(Enum.filter(dice, fn e -> e == 6 end))}
  end

  defp max_faces_count(dice), do: score_upper(dice) |> Map.values |> Enum.max
  defp min_faces_count(dice), do: score_upper(dice) |> Map.values |> Enum.filter( fn e -> e != 0 end) |> Enum.min

   def score_lower(dice) do

    %{"Large Straigh": large_straight(dice),
    "Small straight": small_straight(dice),
    "Full house": full_house(dice),
    "Three of a kind": three_of_a_kind(dice),
    "Four of a kind": four_of_a_kind(dice),
    "Yahtzee": yahtzee_score(dice),
    "Chance": chance(dice)}
    
   end

   defp four_of_a_kind(dice) do
    if max_faces_count(dice) == 4
      do Enum.sum(dice)
   else 0
   end
  end

   defp three_of_a_kind(dice) do
    if max_faces_count(dice) == 3 
      do Enum.sum(dice)
   else 0
   end
  end

   defp full_house(dice) do
     if max_faces_count(dice) == 3 && min_faces_count(dice) == 2
      do 25
   else 0
   end
  end

  defp large_straight(dice) do
    if Enum.sort(dice) == [2,3,4,5,6] || Enum.sort(dice) == [1,2,3,4,5]
      do 40
  else 0
    end
  end

   defp small_straight(dice) do
    smallStraightEnum = Enum.sort(Enum.uniq(dice))
    if smallStraightEnum == [1,2,3,4] || smallStraightEnum == [2,3,4,5] || smallStraightEnum == [3,4,5,6] 
    || smallStraightEnum == [1,3,4,5,6] || smallStraightEnum == [1,2,3,4,6]
      do 30
   else 0
    end
  end

  defp yahtzee_score(dice) do
    sumdice = length(Enum.uniq(dice))
    if sumdice == 1
      do 50
  else 0
    end
  end

  defp chance(dice) do
    if Enum.uniq(dice) |> Enum.flat_map(fn x-> [Enum.count(dice, fn b -> x == b end)] end) |> Enum.sort == [1,2,2]
     do Enum.sum(dice)
  else 0
    end
  end
end
