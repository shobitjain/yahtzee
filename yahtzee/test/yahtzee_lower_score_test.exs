defmodule YahtzeeLowerScoreTest do
  use ExUnit.Case

  def generate(dice_face, occurrences) do
    Enum.to_list(1..6)
    |> List.delete(dice_face)
    |> Enum.shuffle
    |> Enum.take(5 - occurrences)
    |> Enum.concat(List.duplicate(dice_face, occurrences))
    |> Enum.shuffle
  end

  test "Identify 'Three of a kind' with ones" do
    dices = generate(1, 3)
    sum = Enum.sum(dices)
    assert %{"Three of a kind": ^sum} = Yahtzee.score_lower(dices)
  end

  test "Identify 'Three of a kind' with all the others" do
    Enum.map(2..6, fn (dice_face) ->
      dices = generate(dice_face, 3)
      sum = Enum.sum(dices)
      assert %{"Three of a kind": ^sum} = Yahtzee.score_lower(dices)
    end)
  end

  test "Identify 'Four of a kind' with every face" do
    Enum.map(1..6, fn (dice_face) ->
      dices = generate(dice_face, 4)
      sum = Enum.sum(dices)
      assert %{"Four of a kind": ^sum} = Yahtzee.score_lower(dices)
    end)
  end

  test "Identify 'Full house' with every face" do
    Enum.map(1..6, fn _ ->
      [x,y] =
        Enum.shuffle(1..6)
        |> Enum.take(2)
      assert %{"Full house": 25} = Yahtzee.score_lower([x,x,x,y,y] |> Enum.shuffle)
    end)
  end

   test "Identify 'Small straight" do
     Enum.map(1..3, fn x ->
       [a,b,c,d] = Enum.shuffle(x .. x + 3)
       i = []
       i = if x == 1 do [6] else if x == 3 do [1] else [] end end
       m = [a,b,c,d] ++ i
       e = Enum.random(m)
       assert %{"Small straight": 30} = Yahtzee.score_lower([a,b,c,d,e] |> Enum.shuffle)
     end)
   end

  test "Large Straigh case" do
    Enum.map(1..2, fn x ->
      [a,b,c,d,e] = Enum.shuffle(x .. x + 4)
      assert %{"Large Straigh": 40} = Yahtzee.score_lower([a,b,c,d,e] |> Enum.shuffle)
    end)
  end

  test "Identify 'Yahtzee'" do
    Enum.map(1..6, fn n ->
      myroll = List.duplicate(n,5)
      assert %{"Yahtzee": 50} = Yahtzee.score_lower(myroll)
    end)
  end

  test "Identify any other combination" do
    Enum.map(1..6, fn _ ->
      [x,y,z] =
        Enum.shuffle(1..6)
        |> Enum.take(3)
      seq = Enum.shuffle([x,x,y,y,z])
      sum = Enum.sum(seq)
      assert %{Chance: ^sum} = Yahtzee.score_lower(seq)
    end)
  end

  test "Special Case" do
      dices = [2,2,5,5,5]
      sum = Enum.sum(dices)
      assert %{"Three of a kind": ^sum,"Full house": 25} = Yahtzee.score_lower(dices)
  end
end
